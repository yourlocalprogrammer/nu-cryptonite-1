# **CCA2 Encryption**

## Synopsis

Build a public key crypto-system using a key encapsulation mechanism. The idea is that by using a hybrid encryption scheme (combining an asymmetric and symmetric system), we can produce a highly efficient public-key system, thus getting the best of both worlds.
---

## Goals for the student

1. Understand different security definitions for crypto-systems.
2. Hands on experience programming with a variety of crypto building blocks (symmetric encryption, asymmetric encryption, hashing, Message Authentication Code(MAC)…).
---

# The cryptosystem
## CCA2 symmetric encryption
Build CCA2 symmetric encryption from the weaker assumption of CPA encryption


## KEM to make it public-key
 Create a random key for the hybrid encryption scheme, encrypt the message you want to send, and then send it, along with a public-key encryption of the symmetric key.

---
### References
1. (rsa_generate_key(3) - Linux man page)[https://www.geocities.ws/rahuljg/RSA_Algorithm.htm]
2. (Pseudorandom functions and permutations)[http://www.crypto-it.net/eng/theory/prf-and-prp.html]
3. 
